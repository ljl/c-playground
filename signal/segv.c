#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <stdint.h>

	char *ptr = NULL;
void segv_handler(int signum) {
	perror("Segment fault.");
	fprintf(stderr, "func: %s() line: %d\n", __func__, __LINE__);
	//exit(EXIT_FAILURE);
	ptr = "123";
}

int main(int argc, char *argv[])
{
	if(signal(SIGSEGV, segv_handler) == SIG_ERR) {
		fprintf(stderr, "Cannot handle SIGSEGV!\n");
		exit(-1);
	}


	/*
	if(ptr == NULL) {
		printf("OK\n");
	} else {
		printf("Failed!\n");
	}
	*/
	printf("%0x\n", *ptr);

	return 0;
}
