#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/fs.h>

int Daemon(void) {
	pid_t pid;
	int 	i;

	//Create new process
	pid = fork();
	if (pid == -1) {
		return -1;
	} else if (pid != 0) {
		//parent quit
		exit(EXIT_SUCCESS);
	}

	//Create new session and process group
	if (setsid() == -1) {
		return -1;
	}

	//Set the working directory to the root
	if (chdir("/") == -1) {
		return -1;
	}

	//Close open file
	close(0);

	//Redirect fd:0, 1, 2 to /dev/null
	open("/dev/null", O_RDWR);
	dup(0);
	dup(0);

	return 0;
}

int main(int argc, char *argv[])
{
	//1 diy
	Daemon();
	//2 Use glibc func
	//daemon(0, 0);
	while(1);
}
