#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#ifdef GDB
void sig_handler(int signum)
{
	system("gdb ./a.out");
	exit(EXIT_FAILURE);
}
#endif /* GDB */

int main(int argc, char *argv[])
{
#ifdef GDB
	signal(SIGSEGV, sig_handler);
#endif /* GDB */

	int *p = (int *)0xC0000FFF;
	*p = 10;

	return 0;
}
