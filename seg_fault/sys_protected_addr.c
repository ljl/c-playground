#include <stdio.h>

int main(int argc, char *argv[])
{
	/* Trying to write data to address 0*/
#if 0
	int i = 0;
	scanf("%d", i); //i -> &i	
	printf("%d\n", i);
#endif

#if 1
	char *p;
	p = NULL;
	*p = 'x';
	printf("%c", *p);
#endif
	return 0;
}
