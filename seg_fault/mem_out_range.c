#include <stdio.h>

int main(int argc, char *argv[])
{
#if 1 /* Array out of range */
	char test[1];
	printf("%c ", test[100000]);
#endif

#if 0 /* Address isn't available */
	int b = 10;
	printf("%s\n", b); 

	/*Note:
	 * 在打印字符串的时候，实际上是打印某个地址开始的所有字符，但是当你想把整数当字符串打印的时候，这个整数被当成了一个地址，然后printf从这个地址开始去打印字符，直到某个位置上的值为\0。所以，如果这个整数代表的地址不存在或者不可访问，自然也是访问了不该访问的内存——segmentation fault。
	 * */
#endif

#if 0
	char c = 'c';
	int i = 10;
	char buf[100];
	//printf("%s", c); //segment fault
	//printf("%s", i); //segment fault
	//memset(buf, 0, 100);
	//sprintf(buf, "%s", c);	//char -> char array
	memset(buf, 0, 100);
	sprintf(buf, "%s", i);	//int -> char array
#endif
	return 0;
}
