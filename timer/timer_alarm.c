#include <time.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void alarm_handler(int signum) {
	printf("5s passed!\n");
}

void func(void) {
	signal(SIGALRM, alarm_handler);
	alarm(5);

	pause();
}

int main(int argc, char *argv[])
{
	func();
	return 0;
}
