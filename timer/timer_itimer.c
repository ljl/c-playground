#include <sys/time.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void alarm_handler(int signum) {
	printf("Timer hit!\n");
}

void func(void) {
	struct itimerval delay;
	int ret;
	signal(SIGALRM, alarm_handler);

	delay.it_value.tv_sec = 5;
	delay.it_value.tv_usec = 0;
	delay.it_interval.tv_sec = 1;
	delay.it_interval.tv_usec = 0;
	ret = setitimer(ITIMER_REAL, &delay, NULL);
	if(ret) {
		perror("setitimer");
		return;
	}

	pause();
}

int main(int argc, char *argv[])
{
	func();
	return 0;
}
