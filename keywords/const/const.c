#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	int val1 = 1;
	int val2 = 2;
	int val3 = 3;
	const int * foo = &val1;
	int * const bar = &val2;

	printf(" --Origin--\n");
	printf(" const int * = %p, %d\n", foo, *foo);
	printf(" int * const = %p, %d\n", bar, *bar);

	printf(" --Raw Data Modified--\n");
	val1 = 10;
	val2 = 20;
	printf(" const int * = %p, %d\n", foo, *foo);
	printf(" int * const = %p, %d\n", bar, *bar);

	printf(" --Data Modified By Pointer--\n");
	//*foo = 100; //ERROR
	*bar = 200;
	printf(" const int * = %p, %d\n", foo, *foo);
	printf(" int * const = %p, %d\n", bar, *bar);

	printf(" --Pointer Modified--\n");
	//*foo = &val3; //ERROR
	*bar = &val3; 
	printf(" const int * = %p, %d\n", foo, *foo);
	printf(" int * const = %p, %d\n", bar, *bar);
	return 0;
}
