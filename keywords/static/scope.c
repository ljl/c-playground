#include <stdio.h>
#include <string.h>

/*
 * Usage 1：
 * 函数内部声明为static，可作为对象间的一种通信机制。
 * 如果一个局部变量被声明为static，那么将只有唯一的一个静态分配的对象，
 * 它被用于在该函数的所有调用中表示这个变量。这个对象只有在执行线程第
 * 一次到达它的定义时初始化。
 * 
 * */
void func1(void) {
	static int counter;

	counter++;
	printf("%d\n", counter);
}

int main(int argc, char *argv[])
{
	func1();	
	func1();	
	func1();	
	func1();	

	return 0;
}
