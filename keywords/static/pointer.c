#include <stdio.h>

//function returns address of local variable
int * func(void) {
	//int val; 		//return error pointer
	static int val;
	val++;	

	return &val;
}

int main(int argc, char *argv[])
{
	printf("%d\n", *func());
	return 0;
}
