#include <stdio.h>

void func0(void) {
	printf("%s %s\n", __FILE__, __func__);
}

void func2(void) {
	printf("%s %s\n", __FILE__, __func__);
}

int main(int argc, char *argv[])
{
	func1();
	func2();
	//func0.c定义了func0但对外隐藏，如果本文件未定义func0，则编译报错未定义
	func0();

	return 0;
}
