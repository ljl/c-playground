#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "slist.h"

struct node_t *node_init(struct node_t *node) {
	node->data = 0;
	node->next = NULL;
	return node;
}	

struct node_t *list_add(struct node_t *head, struct node_t *node) {
	node->next = head->next;
	head->next = node;
	return node;
}

struct node_t *list_add_tail(struct node_t *head, struct node_t *node) {
	struct node_t *cur= head;
	while(cur->next != NULL) {
		cur = cur->next;
	}
	cur->next = node;
	return node;
}

struct node_t *list_traverse(struct node_t *head, 
		void (*todo)(struct node_t *)) {
	struct node_t *cur = head->next;
	while(cur != NULL) {
		todo(cur);
		cur = cur->next;
	}
	printf("\n");

	return head;
}

bool list_del(struct node_t *head, struct node_t *node) {
	struct node_t *cur = head;
	while(cur->next != NULL) {
		if(cur->next == node) { //Found target 
			cur->next = node->next;
			node->next = NULL;
			return true;
		}
		cur = cur->next;
	}
	return false;
}

int list_is_empty(struct node_t *head) {
	return head->next == NULL;
}

void list_destroy(struct node_t *head) {
	while(!list_is_empty(head)) {
		list_del(head, head->next);
	}
	head->next = NULL;
	free(head);
}

void print_node(struct node_t *node) {
	printf("%d ", node->data);
}

