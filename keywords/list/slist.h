#ifndef _SLIST_H_
#define _SLIST_H_

typedef size_t element_t;
struct node_t {
	element_t data;
	struct node_t *next;
};

struct node_t *node_init(struct node_t *node);
struct node_t *list_add(struct node_t *head, struct node_t *node);
struct node_t *list_add_tail(struct node_t *head, struct node_t *node);
struct node_t *list_traverse(struct node_t *node, 
		void (*todo)(struct node_t *));
bool list_del(struct node_t *head, struct node_t *node);
int list_is_empty(struct node_t *head);
void list_destroy(struct node_t *head);
void print_node(struct node_t *node);

#endif /* _SLIST_H_ */
