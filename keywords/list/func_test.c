#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "slist.h"


int main(int argc, char *argv[])
{
	struct node_t *head = (struct node_t *)malloc(sizeof(struct node_t));
	if(!head) {
		fprintf(stderr,"Out of memory!\n");
	}
	node_init(head);

	struct node_t s[] = {
		{0},
		{1},
		{2},
		{3},
		{4},
		{5},
		{6},
		{7},
		{8},
		{9}
	};

	size_t i;
	for(i = 0; i < sizeof(s) / sizeof(s[0]); i++) {
		//list_add(head, s + i);
		list_add_tail(head, s + i);
	}
	list_traverse(head, print_node);

	printf("del %s!\n", list_del(head, s + 9) ? "succeed" : "failed");
	list_traverse(head, print_node);

	struct node_t val = {
		.data = 12,
	};
	list_add(head, &val);
	list_traverse(head, print_node);

	list_destroy(head);

	return 0;
}
