#include "slist.h"

struct node_t *list_is_circle(struct node_t *head) {
	if(!head) {
		return NULL;
	}

	node_t *p1 = head;
	node_t *p2 = head;

	do {
		if (p1->next != NULL &&	p2->next != NULL &&
				p2->next->next != NULL) {
			p1 = p1->next;
			p2 = p2->next->next;
		}
		else
			return NULL;
	}
	while(p1 != p2);

	p2 = head;
	while(p1 != p2) {
		p1 = p1->next;
		p2 = p2->next;
	}

	return p1;
}

