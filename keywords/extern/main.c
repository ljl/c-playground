#include <stdio.h>

//extern char *a;		//segment fault
extern char a[];
char *b = a;
int main(int argc, char *argv[])
{
	printf("%s\n", a);
	printf("%s\n", b);
	return 0;
}
