#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <iostream>

using namespace std;
#define MAX_EVENTS		500
struct myevent_s {
	int fd;
	void (*call_back)(int fd, int events, void *arg);
	int events;
	void *arg;
	int status;
	char buffer[128];
	int len, s_offset;
	long last_active;
};

void event_set(myevent_s *ev, int fd, void (*call_back)(int, int, void*), void *arg) {
	ev->fd = fd;
	ev->call_back = call_back;
	ev->events = 0;
	ev->arg = arg;
	ev->status = 0;
	bzero(ev->buffer, sizeof(ev->buffer));
	ev->s_offset = 0;
	ev->len = 0;
	ev->last_active = time(NULL);
}

void event_add(int epoll_fd, int events, myevent_s *ev) {
	struct epoll_event epv = {0, {0}};
	int op;
	epv.data.ptr = ev;
	epv.events = ev->events = events;
	if(1 == ev->status) {
		op = EPOLL_CTL_MOD;
	} else {
		op = EPOLL_CTL_ADD;
		ev->status = 1;
	}

	if(epoll_ctl(epoll_fd, op, ev->fd, &epv) < 0) {
		printf("fd[%d], events[%d] add failed!\n", ev->fd, events);
	} else {
		printf("fd[%d], op = %d, events[%0X] add ok!\n", ev->fd, op, events);
	}
}

void event_del(int epoll_fd, myevent_s *ev) {
	struct epoll_event epv = {0, {0}};
	if(ev->status != 1) return;
	epv.data.ptr = ev;
	ev->status = 0;
	epoll_ctl(epoll_fd, EPOLL_CTL_DEL, ev->fd, &epv);
}

int g_epoll_fd;
myevent_s g_events[MAX_EVENTS + 1];

void recv_data(int fd, int events, void *arg);
void send_data(int fd, int events, void *arg);

void accept_conn(int fd, int events, void *arg) {
	struct sockaddr_in sin;
	socklen_t len = sizeof(struct sockaddr_in);
	int new_fd, i;
	if((new_fd = accept(fd, (struct sockaddr*)&sin, &len)) == -1) {
		if(errno != EAGAIN && errno != EINTR) {
		}
		printf("%s: accept, %d", __func__, errno);
		return ;
	}
	do {
		for(i = 0; i < MAX_EVENTS; i++) {
			if(g_events[i].status == 0) break;
		}

		if(i == MAX_EVENTS) {
			printf("%s: max connection limit[%d].", __func__, MAX_EVENTS);
			break;
		}

		event_set(&g_events[i], new_fd, recv_data, &g_events[i]);
		event_add(g_epoll_fd, EPOLLIN, &g_events[i]);
	} while(0);
	printf("new conn[%s: %d] [time: %d], pos[%d] \n", 
			inet_ntoa(sin.sin_addr),
			ntohs(sin.sin_port),
			g_events[i].last_active, i);
}

void recv_data(int fd, int events, void *arg) {
	struct myevent_s *ev = (struct myevent_s *)arg;
	int len;
	len = recv(fd, ev->buffer + ev->len, sizeof(ev->buffer) - 1 - ev->len, 0);
	event_del(g_epoll_fd, ev);
	if(len > 0) {
		ev->len += len;
		ev->buffer[len] = '\0';
		printf("C[%d]: %s\n", fd, ev->buffer);
		event_set(ev, fd, send_data, ev);
		event_add(g_epoll_fd, EPOLLOUT, ev);
	} else if(0 == len) {
		close(ev->fd);
		printf("[fd = %d] pos[%d], closed\n", fd, ev->events);
	}
	else {
		close(ev->fd);
		printf("recv [fd = %d] error[%d]: %s\n", fd, errno, strerror(errno));
	}
}

void send_data(int fd, int events, void *arg) {
	struct myevent_s *ev = (struct myevent_s*)arg;
	int len;
	len = send(fd, ev->buffer + ev->s_offset, ev->len - ev->s_offset, 0);
	if(len > 0) {
		printf("send [fd = %d], [%d <-> %d]%s\n", fd, len, ev->len, ev->buffer);
		ev->s_offset += len;
		if(ev->s_offset == ev->len) {
			event_del(g_epoll_fd, ev);
			event_set(ev, fd, recv_data, ev);
			event_add(g_epoll_fd, EPOLLIN, ev);
		}
	} else {
		close(ev->fd);
		event_del(g_epoll_fd, ev);
		printf("send [fd = %d] error[%d]\n", fd, errno);
	}
}

void init_listen_sock(int epoll_fd, short port) {
	int listen_fd = socket(AF_INET, SOCK_STREAM, 0);
	fcntl(listen_fd, F_SETFL, O_NONBLOCK);
	printf("server listen fd = %d\n, listen_fd");
	event_set(&g_events[MAX_EVENTS], listen_fd, accept_conn, &g_events[MAX_EVENTS]);
	event_add(epoll_fd, EPOLLIN, &g_events[MAX_EVENTS]);
	sockaddr_in sin;
	bzero(&sin, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = INADDR_ANY;
	sin.sin_port = htons(port);
	bind(listen_fd, (const sockaddr *)&sin, sizeof(sin));
	listen(listen_fd, 5);
}

int main(int argc, char *argv[])
{
	unsigned short port = 12345;
	if(argc == 2) {
		port = atoi(argv[1]);
	}

	g_epoll_fd = epoll_create(MAX_EVENTS);
	if(g_epoll_fd <= 0) {
		printf("create epoll failed. %d\n", g_epoll_fd);
	}
	init_listen_sock(g_epoll_fd, port);
	struct epoll_event events[MAX_EVENTS];
	printf("server running:port[%d]\n", port);
	int check_pos = 0;
	while(1) {
		long now = time(NULL);
		for(int i = 0; i < 100; i++, check_pos++) {
			if(check_pos == MAX_EVENTS) check_pos = 0;
			if(g_events[check_pos].status != 1) continue;
			long duration = now - g_events[check_pos].last_active;
			if(duration >= 60) {
				close(g_events[check_pos].fd);
				printf("[fd = %d] timeout[%d -- %d].\n",
						g_events[check_pos].fd, g_events[check_pos].last_active, now);
				event_del(g_epoll_fd, &g_events[check_pos]);
			}
		}

		int fds = epoll_wait(g_epoll_fd, events, MAX_EVENTS, 1000);
		if(fds < 0) {
			printf("epoll_wait error, exit\n");
			break;
		}

		for(int i = 0; i < fds; i++) {
			myevent_s *ev = (struct myevent_s*)events[i].data.ptr;
			if((events[i].events & EPOLLIN) && (ev->events & EPOLLIN)) {
				ev->call_back(ev->fd, events[i].events, ev->arg);
			}
			if((events[i].events & EPOLLOUT) && (ev->events & EPOLLOUT)) {
				ev->call_back(ev->fd, events[i].events, ev->arg);
			}
		}
	}


	return 0;
}
