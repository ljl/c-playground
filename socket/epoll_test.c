#include <sys/epoll.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <strings.h>
#include <stdlib.h>
#include <string.h>

#define EPOLL_NUM 100
static int setnonblocking(int fd) {
	int flag, s;
	flag = fcntl(fd, F_GETFL, 0);
	if(flag == -1) {
		perror("fcntl");
		return -1;
	}

	flag |= O_NONBLOCK;
	s = fcntl(fd, F_SETFL, flag);
	if(s == -1) {
		perror("fcntl");
		return -1;
	}

	return 0;
}

int main(int argc, char *argv[])
{
	int i, listenfd, connfd, sockfd, epfd, nfds, port;
	ssize_t n;
	char str[100];
	socklen_t clientlen;

	if(2 == argc) {
		if((port == atoi(argv[1])) < 0) {
			fprintf(stderr, "Usage: %s port\n", argv[0]);
			return 1;
		}
	} else {
		fprintf(stderr, "Usage: %s port\n", argv[0]);
		return 1;
	}

	struct epoll_event ev, events[20];
	//create epoll 
	epfd = epoll_create(256);
	if(epfd < 0) {
		perror("epoll_create");
		exit(1);
	}
/*Part 2*/
	struct sockaddr_in clientaddr;
	struct sockaddr_in serveraddr;
	listenfd = socket(AF_INET, SOCK_STREAM, 0);

	ev.data.fd = listenfd;
	ev.events = EPOLLIN | EPOLLET;

	//epoll event register
	//EPOLL_CTL_ADD: register new fd to epfd
	//EPOLL_CTL_MOD: modify registered listen event
	//EPOLL_CTL_DEL: del a fd from epfd
	epoll_ctl(epfd, EPOLL_CTL_ADD, listenfd, &ev);
	bzero(&serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	char *local_addr = "127.0.0.1";
	inet_aton(local_addr, &(serveraddr.sin_addr));
	serveraddr.sin_port = htons(port);
	bind(listenfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr));
	listen(listenfd, 20);

	while(1) {
		nfds = epoll_wait(epfd, events, 20, 500);
		for(i = 0; i < nfds; ++i) {
			if(events[i].data.fd == listenfd) {
				connfd = accept(listenfd, (struct sockaddr *)&clientaddr, &clientlen);
				if(connfd < 0) {
					perror("connfd < 0");
					exit(1);
				}

				char *str2 = inet_ntoa(clientaddr.sin_addr);
				printf("accept a connection from %s\n", str2);
				ev.data.fd = connfd;
				ev.events = EPOLLIN | EPOLLET;
				epoll_ctl(epfd, EPOLL_CTL_ADD, connfd, &ev);
			} else if(events[i].events & EPOLLIN) {
				//Already connected, data recved
				printf("EPOLLIN\n");
				if((sockfd = events[i].data.fd) < 0) {
					continue;
				}
				if((n = read(sockfd, str, 100)) < 0) {
					if(errno == ECONNRESET) {
						close(sockfd);
						events[i].data.fd = -1;
					} else {
						printf("read error\n");
					}
				} else if(n == 0) {
					close(sockfd);
					events[i].data.fd = -1;
				}
				str[n] = '\0';
				printf("read %s\n", str);
				ev.data.fd = sockfd;
				ev.events = EPOLLOUT | EPOLLET;
			} else if(events[i].events & EPOLLOUT) {
				sockfd = events[i].data.fd;
				write(sockfd, str, n);
				ev.data.fd = sockfd;
				ev.events = EPOLLIN | EPOLLET;
				epoll_ctl(epfd, EPOLL_CTL_MOD, sockfd, &ev);
			}
		}
	}

	return 0;
}
