#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h> //usr/include/netinet/in.h struct sockaddr_in。linux/in.h头文件中没有定义in_addr_t类型，和inet_addr函数不兼容，所以用了netinet/in.h来取代
#include <arpa/inet.h> //usr/include/arpa/inet.h大小端转换
#include <sys/select.h>

#include <string.h>

//#include "chat.h"
#include "protocol.h"

//目标服务器的ip地址
#define NET_PORT 1800
#define IP_ADDR "127.0.0.1"

#define DEFAULT_NAME "anonymous"

int main(int argc, char **argv)
{
	char self_name[CLIENT_NAME_SIZE] = DEFAULT_NAME;

	if (argc == 1) {
	} else if (argc == 2) {
		strncpy(self_name, argv[1], sizeof(self_name));
	} else {
		fprintf(stderr, "wrong argu\n");
		goto err_argc;
	}

	int dst_sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (dst_sockfd < 0) {
		perror("socket");
		goto err_socket;
	}

	//目标服务器的属性
	struct sockaddr_in attr = {
		.sin_family = AF_INET,
		.sin_port = htons(NET_PORT),
		.sin_addr = {
			//inet_addr已经做过大小端转换
			.s_addr = inet_addr(IP_ADDR),
		},
	};

	if (connect(dst_sockfd, (struct sockaddr*)&attr, sizeof(attr)) < 0) {
		perror("connect");
		goto err_connect;
	}

	//将本客户端的id发给服务器
	write(dst_sockfd, self_name, sizeof(self_name));

	//===========select==============

	struct msg_info msg = {0};
	fd_set fds;
	struct timeval timeout;
	int ret_sel;

	while (1) {
		//select执行前必须重新初始化集合和timeval，因为select执行时会破坏它们

		//清空集合
		FD_ZERO(&fds);
		//添加需要监控的文件描述符到集合中
		FD_SET(dst_sockfd, &fds);
		FD_SET(STDIN_FILENO, &fds);
		timeout.tv_sec = 2;
		timeout.tv_usec = 0;

		//第一个参数nfds是所有监控的文件描述符中，数字最大的一个+1
		//后面三个参数是监控的文件描述符的集合，按照数据传输的方向分成3类。监控中的任何一个文件描述符被唤醒的话，都会造成select的返回，一次返回可能包含多个文件描述符的唤醒
		//在select返回后，集合中只剩下被唤醒的文件描述符，没有被唤醒的文件描述符会被select删除出集合。换句话说，select执行后，集合就被破坏了，每次执行select时都要重新初始化
		//最后一个参数表示超时时间，超过这个时间还没有监控中的文件描述符被唤醒的话，select会返回
		//返回值就是被唤醒的文件描述符的个数，如果是因为timeout返回的，说明没有文件描述符被唤醒，当然就返回0
		ret_sel = select(dst_sockfd+1, &fds, NULL, NULL, &timeout);

		switch (ret_sel) {
			case -1://有错误
				perror("select");
				goto err_select;
			case 0://没有文件描述符唤醒，是timeout造成的函数返回
				continue;
			default: //有文件描述符被唤醒了，select的返回值就是唤醒的文件描述符的数量

	if (FD_ISSET(STDIN_FILENO, &fds)) {
		//键盘有输入，标准输入有数据获得，此时再使用read读取，不可能会挂起
		read(STDIN_FILENO, &msg, sizeof(msg));
		write(dst_sockfd, &msg, sizeof(msg));
		memset(&msg, 0, sizeof(msg));
	}

	if (FD_ISSET(dst_sockfd, &fds)) {
		if (!read(dst_sockfd, &msg, sizeof(msg))) {
			//对方关闭连接
			printf("server disconnected\n");
			goto server_disconnect;
		} else {
			printf("%d: %d", msg.addr, msg.cmdtype);
			memset(&msg, 0, sizeof(msg));
		}
	}

		}

	}

server_disconnect:
	close(dst_sockfd);

	return 0;

err_select:
err_connect:
	close(dst_sockfd);
err_socket:
err_argc:
	return -1;
}
