#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
//#include <linux/in.h> //usr/include/linux/in.h
#include <netinet/in.h> //usr/include/netinet/in.h struct sockaddr_in。linux/in.h头文件中没有定义in_addr_t类型，和inet_addr函数不兼容，所以用了netinet/in.h来取代
#include <arpa/inet.h> //usr/include/arpa/inet.h大小端转换

#include <string.h>
#include <stdlib.h>
#include <signal.h>

#include "list.h"
//#include "chat.h"
#include "protocol.h"

#define NET_PORT 1800
#define IP_ADDR "127.0.0.1"
#define BACK_LOG 3

static int servfd;

static void sig_handler(int num)
{
	close(servfd);
	exit(0);
}

struct fd_info {
	int fd; //用于排序的成员
	struct list_head entry;
	char name[CLIENT_NAME_SIZE];
};

//带有排序属性的插入节点操作
/* @node: 新插入的节点
 * @head: 链表的头节点
 * @cmp: 回调函数，用于比较两个节点的大小，返回值<0,=0,>0
 */
static void insert_node_sort(struct list_head *node,
		struct list_head *head,
		int (*cmp)(struct list_head *,
			struct list_head *))
{	
	struct list_head *cur = NULL;
	list_for_each(cur, head) {
		if (cmp(node, cur) < 0) {
			break;
		}
	}
	//无论是找到插入点还是链表遍历完毕将节点插到最后，插入把新节点的操作都归为了将节点插入到cur前面
	__list_add(node, cur->prev, cur);
}

static int cmp_node(struct list_head *a, struct list_head *b)
{
	struct fd_info *pa = container_of(a, struct fd_info, entry);
	struct fd_info *pb = container_of(b, struct fd_info, entry);
	return pa->fd - pb->fd;
}



int main(int argc, char *argv[])
{
	signal(SIGINT, sig_handler);

	int serv_sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (serv_sockfd < 0) {
		perror("socket");
		goto err_socket;
	}

	servfd = serv_sockfd;

	struct sockaddr_in attr = {
		.sin_family = AF_INET,
		.sin_port = htons(NET_PORT),
		.sin_addr = {
			//inet_addr已经做过大小端转换
			.s_addr = inet_addr(IP_ADDR),
		},
	};

	if (bind(serv_sockfd, (const struct sockaddr*)&attr, sizeof(struct sockaddr_in)) < 0) {
		perror("bind");
		goto err_bind;
	}

	if (listen(serv_sockfd, BACK_LOG) < 0) {
		perror("listen");
		goto err_listen;
	}

	//===========select==============

	//为select准备变量
	fd_set fds;
	struct timeval timeout;
	int ret_sel;
	int clientfd; //临时的客户端套接字
	int fdmax; //文件描述符fds中fd数值最大的一个
	struct fd_info *p = NULL; //临时指针
	struct fd_info *p1 = NULL; //临时指针
	struct fd_info *p2 = NULL; //临时指针

	size_t attr_len = sizeof(attr);
	struct msg_info msg = {0};

	//用于维护所有客户端sockfd的链表
	LIST_HEAD(head);

	while (1) {

		timeout.tv_sec = 2;
		timeout.tv_usec = 0;
		FD_ZERO(&fds);
		//标准输入和服务器端套接字因为fd值固定所以没必要用链表来维护
		FD_SET(STDIN_FILENO, &fds);
		FD_SET(serv_sockfd, &fds);

		//链表上所有节点都是客户端节点
		list_for_each_entry(p, &head, entry) {
			FD_SET(p->fd, &fds);
		}

		//求所监控的fd中数值最大的一个，如果链表为空，则最大值为serv_sockfd（通常是3）,否则应为链表最后一个节点的fd值（链表是按fd的从小到大的顺序排列的）
		if (list_empty(&head)) {
			fdmax = serv_sockfd;
		} else {
			fdmax = container_of(head.prev, struct fd_info, entry)->fd;
		}

		ret_sel = select(fdmax+1, &fds, NULL, NULL, &timeout);

		switch (ret_sel) {
			case -1:
				perror("select");
				goto err_select;
			case 0:
				continue;
			default:

				/*//标准输入
				if (FD_ISSET(STDIN_FILENO, &fds)) {
					//将服务器端标准输入内容广播给所有客户端
					strncpy(msg.name, "server msg", sizeof(msg.name));
					read(STDIN_FILENO, msg.content, sizeof(msg.content));
					list_for_each_entry(p, &head, entry) {
						write(p->fd, &msg, sizeof(msg));
					}
					memset(&msg, 0, sizeof(msg));
				}*/
				
				//有客户端连接请求
				if (FD_ISSET(serv_sockfd, &fds)) {
					clientfd = accept(serv_sockfd, (struct sockaddr*)&attr, &attr_len);
					//为新来的客户端创建一个节点
					p = (struct fd_info*)malloc(sizeof(struct fd_info));
					if (!p) {
						goto err_malloc;
					}
					p->fd = clientfd;
					//读取客户端id
					read(clientfd, p->name, sizeof(p->name));
					//将新节点有序地插入到链表中
					insert_node_sort(&p->entry, &head, cmp_node);
					printf("client connected: ID=%s, IP=%s: %d\n", p->name, inet_ntoa(attr.sin_addr), ntohs(attr.sin_port));
				}

				//客户端有数据发过来，挨个判断所有客户端是否被唤醒
				list_for_each_entry_safe(p, p2, &head, entry) {
					if (FD_ISSET(p->fd, &fds)) {
						if (!read(p->fd, &msg, sizeof(struct msg_info))) {
							//对方关闭连接
							printf("client %s disconnected\n", p->name);
							list_del_init(&p->entry);
							free(p);
						} else {
							//客户端信息打到屏幕上
							printf("client's addr: %d\n", msg.addr);
							write(p->fd, &msg, sizeof(msg));
							fflush(stdout);
						}
					}
				}
		}
	}


	//销毁链表
	list_for_each_entry_safe(p, p1, &head, entry) {
		list_del_init(&p->entry);
		free(p);
	}

	close(clientfd);
	close(serv_sockfd);

	return 0;

err_malloc:
err_select:
err_listen:
err_bind:
	close(serv_sockfd);
err_socket:
	return -1;
}
/*
void handle_msg(struct msg* info)
{
	switch(info->cmdtype)
	{

	}
}
*/

