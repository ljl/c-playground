#pragma once

#define CLIENT_NAME_SIZE 32
#define STDIN_SIZE 80

#define CMD_POWER 				0xC0
#define POWER_SET				0x03
#define POWER_STATUS			0x02
#define POWER_ON				0x01
#define POWER_OFF				0x00

#define CMD_SWITCH				0xC1
#define SWITCH_SET				0x03
#define SWITCH_STATUS			0x02
#define SWITCH_ON				0x01
#define SWITCH_OFF				0x00

#define CMD_TV					0xC2
#define TV_SET					0x03
#define TV_STATUS				0x02
#define TV_ON					0x01
#define TV_OFF 					0x00
#define TV_CHANNEL

#define CMD_DOOR				0xC3
#define DOOR_SET				0x03
#define DOOR_STATUS				0x02
#define DOOR_OPEN				0x01
#define DOOR_CLOSE				0x00

#define CMD_AIR_CONDITIONER		0xC4
#define AIR_CONDITIONER_SET		0x03
#define AIR_CONDITIONER_STATUS	0x02
#define AIR_CONDITIONER_ON		0x01
#define AIR_CONDITIONER_OFF		0x00
#define AIR_CONDITIONER_TEMP

#define CMD_MICROWAVE_OVEN		0xC5
#define MICROWAVE_OVEN_SET		0x03
#define MICROWAVE_OVEN_STATUS	0x02
#define MICROWAVE_OVEN_ON		0x01
#define MICROWAVE_OVEN_OFF		0x00

#define CMD_REFRIGERATOR		0xC6
#define REFRIGERATOR_SET		0x03
#define REFRIGERATOR_STATUS		0x02
#define REFRIGERATOR_ON			0x01
#define REFRIGERATOR_OFF		0x00

#define CMD_WASHER				0xC7
#define WASHER_SET				0x03
#define WASHER_STATUS			0x02
#define WASHER_ON				0x01
#define WASHER_OFF				0x00

//服务器发送给客户端数据的格式
struct server_msg_info {
	unsigned char name[CLIENT_NAME_SIZE];
	unsigned char content[STDIN_SIZE];
};

struct msg_info {
	unsigned char addr;
	unsigned char cmdtype;
	unsigned short data;
	unsigned char crc8;
};

/*
typedef union
{
	struct
	{
		uint8_t rSatus:1;
		uint8_t rSet:1;
		uint8_t _reserved0:6;
	}b;
	uint8_t w;
} REG_Type;
*/
