#pragma once

struct list_head {
	struct list_head *prev;
	struct list_head *next;
};

#define LIST_HEAD(name) struct list_head name = {&(name), &(name)}

static inline void INIT_LIST_HEAD(struct list_head *node)
{
	node->prev = node;
	node->next = node;
}

static inline void __list_add(struct list_head *new_node,
		struct list_head *prev,
		struct list_head *next)
{
	new_node->prev = prev;
	new_node->next = next;
	prev->next = new_node;
	next->prev = new_node;
}

static inline void list_add(struct list_head *new_node,
		struct list_head *head)
{
	__list_add(new_node, head, head->next);
}

static inline void list_add_tail(struct list_head *new_node,
		struct list_head *head)
{
	__list_add(new_node, head->prev, head);
}

static inline void list_del(struct list_head *node)
{
	node->prev->next = node->next;
	node->next->prev = node->prev;
}

static inline void list_del_init(struct list_head *node)
{
	node->prev->next = node->next;
	node->next->prev = node->prev;
	INIT_LIST_HEAD(node);
}

static int list_empty(struct list_head *head)
{
	return head->next == head;
}

#define offsetof(type, member) \
	( (size_t)(&((type*)0)->member) )

#define container_of(ptr, type, member) \
	({ typeof(((type*)0)->member) *__mptr = ptr; \
	 (type*)((char*)__mptr - offsetof(type, member)); })

#define list_for_each(cur, head) \
	for (cur = (head)->next; (cur) != (head); \
		cur = (cur)->next)

#define list_for_each_safe(cur, tmp, head) \
	for (cur = (head)->next, tmp = (cur)->next; \
		(cur) != (head); cur = tmp, tmp = (tmp)->next)

#define list_for_each_reverse(cur, head) \
	for (cur = (head)->prev; (cur) != (head); \
		cur = (cur)->prev)

#define list_for_each_continue(cur, head) \
	for (cur = (cur)->next; (cur) != (head); \
		cur = (cur)->next)

#define list_for_each_from(cur, head) \
	for (; (cur) != (head); cur = (cur)->next)

#define list_for_each_entry(ptr, head, member) \
	for ( ptr = container_of((head)->next, typeof(*(ptr)), member); \
		&(ptr)->member != (head); \
		ptr = container_of((ptr)->member.next, typeof(*(ptr)), member) )

#define list_for_each_entry_safe(ptr, tmp, head, member) \
	for ( ptr = container_of((head)->next, typeof(*(ptr)), member), \
		tmp = container_of((ptr)->member.next, typeof(*(ptr)), member); \
		&(ptr)->member != (head); \
		ptr = tmp, \
		tmp = container_of((tmp)->member.next, typeof(*(ptr)), member) )

