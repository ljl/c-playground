#pragma once

#define CLIENT_NAME_SIZE 32
#define STDIN_SIZE 80

//服务器发送给客户端数据的格式
struct server_msg_info {
	unsigned char name[CLIENT_NAME_SIZE];
	unsigned char content[STDIN_SIZE];
};

struct msg {
	unsigned char addr;
	unsigned char cmdtype;
	unsigned short data;
	unsigned char crc8;
};
